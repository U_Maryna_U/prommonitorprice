from threading import Thread
from bson import ObjectId
from telegram import  KeyboardButton, ReplyKeyboardMarkup
from telegram.ext import Updater, CommandHandler, ConversationHandler, MessageHandler, Filters, CallbackQueryHandler
import requests
import re
from bs4 import BeautifulSoup
import pymongo
import time
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
import os
PORT = int(os.environ.get('PORT', 80))

client = pymongo.MongoClient(
    "mongodb+srv://promMonitoringUser:20PromMonitoring21@prommonitoring.a6cmd.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
db = client["priceMonitoring"]

keyboard = [[KeyboardButton("/help")], [KeyboardButton("/add_product")],
            [KeyboardButton("/delete_product")]]
reply_markup = ReplyKeyboardMarkup(keyboard)

def send_welcome(update, context):
    reply_message = """ 
        Використовуй команду /add_product для додавання товару і надсилай посилання на товар
    Для видалення товару використовуй команду /delete_product та обирай товар зі списку.
    Для отримання детальних використовуй команду /help"""
    update.message.reply_text(reply_message, reply_markup= reply_markup)
    return ConversationHandler.END


def add_product(update, context):
    update.message.reply_text("Надішли мені посилання на продукт", reply_markup= reply_markup)
    return 0


def get_price_by_url(update, element):
    page = requests.get(update.message.text)
    soup = BeautifulSoup(page.text, "html.parser")
    if  soup.find(element, {"data-qaid": "product_price"}) is not None:
        return soup.find(element, {"data-qaid": "product_price"})['data-qaprice']
    else:
        return None

def get_name_by_url(update, element):
    page = requests.get(update.message.text)
    soup = BeautifulSoup(page.text, "html.parser")
    if soup.find(element, {"data-qaid": "product_name"}) is not None:
        return soup.find(element, {"data-qaid": "product_name"}).text
    else:
        return None


def monitoring(update):
    try:
        col = db[str(update.message.chat.id)]
        query = col.find({"url": str(update.message.text)})
        while query is not None:
            query = col.find_one({"url": update.message.text})
            price = get_price_by_url(update, "span")
            if query['price'] != price:
                col.update_one({"url": update.message.text}, {"$set": {"price": str(price)}})
                update.message.reply_text("Ціна товару " + get_name_by_url(update, "h1") \
                                 + " змінилась з "+ query['price']+" на " + price, reply_markup= reply_markup)
                update.message.reply_text(update.message.text, reply_markup= reply_markup)
            time.sleep(10)
    except:
        update.message.reply_text( "Щось пішло не так з моніторингом ціни" + get_name_by_url(update, "h1") +
                         " Спробуйте видалити товар і знову додати даний товар.", reply_markup= reply_markup)


def adding_to_db(update):
    try:
        col = db[str(update.message.chat.id)]
        price = get_price_by_url(update, "span")
        name = get_name_by_url(update, "h1")
        if price is not None and name is not None:
            col.update_one({"url": update.message.text}, { "$set": {"url": update.message.text, "name": name, "price": price}}, upsert=True)
            return True
        else:
            return False
    except:
        update.message.reply_text("Не вдалось додати товар "+ get_name_by_url(update, "h1")
                         + " cпробуйте додати його знову трохи пізніше, а також перевірте коректність роботи посилання.", reply_markup= reply_markup)
        return ConversationHandler.END


def add_product_by_url(update, context):
 try:
    if re.match(r"""(?i)\b((?:https?:(?:/{1,3}|[a-z0-9%])|[a-z0-9.\-]+[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)/)(?:[^\s()<>{}\[\]]+|\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\))+(?:\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’])|(?:(?<!@)[a-z0-9]+(?:[.\-][a-z0-9]+)*[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)\b/?(?!@)))""", update.message.text):
        if adding_to_db(update):
            update.message.reply_text( "Товар "+get_name_by_url(update, "h1") \
                             + " успішно доданий до переліку товарів. Його поточна ціна становить "+ get_price_by_url(update, "span") + " грн.", reply_markup= reply_markup)
            #yield ConversationHandler.END
            #_thread.start_new_thread(monitoring,(update))
            thread = Thread(target=monitoring, args=(update,))
            return ConversationHandler.END
        else:
            raise Exception("")
    else:
        if update.message.text == "/help":
            return send_welcome(update)
            #return ConversationHandler.END

        else:
            update.message.reply_text( "Надісланий текст не посилання.", reply_markup= reply_markup)
            return add_product(update, context)



 except:
     update.message.reply_text("Не вдалось додати товар. Cпробуйте додати його знову трохи пізніше, а також перевірте коректність роботи посилання.", reply_markup= reply_markup)
     return ConversationHandler.END

def cancel(update, context):
   update.message.reply_text("Спробуйте додати товар в інший раз", reply_markup= reply_markup)
   return ConversationHandler.END




def delete_product(update, context):
    col = db[str(update.message.chat.id)]
    names = []
    for i in col.find({}):
        names.append([i['name'], str(i['_id'])])
    if len(names) == 0:
       update.message.reply_text("Ви ще не додали жодного товару для моніторингу", reply_markup= reply_markup)
    else:
        keyboard1 = []
        for i in names:
            keyboard1.append([InlineKeyboardButton(i[0], callback_data= i[1])])
        reply_markup1 = InlineKeyboardMarkup(keyboard1)
        #keyboard = keyboa_maker(items=names, items_in_row= 1, front_marker= "$del=")
        update.message.reply_text( "Обери товар для видалення", reply_markup=reply_markup1)

def test_callback(update, context):
    try:
        col = db[str(update.callback_query.message.chat.id)]
        names = col.find({"_id": update.callback_query.data.replace("$del=",'')})
        name = ""
        for i in names:
            name = i['name']
        col.delete_one({"_id": ObjectId(update.callback_query.data.replace("$del=", ''))})
        update.callback_query.message.reply_text( "Товар " + name +" успішно видалений зі списку товарів для моніторингу", reply_markup= reply_markup)
    except:
        update.message.reply_text("Виникли проблеми з видаленням даного товару. Спробуйте видалити його пізніше.", reply_markup= reply_markup)



if __name__ == '__main__':
    updater = Updater('1761092092:AAFrlihrNEI5akgickPXRvANRu7GUaWPVDE')
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start', send_welcome))
    dp.add_handler(CommandHandler('help', send_welcome))
    adding = ConversationHandler(entry_points=[CommandHandler('add_product', add_product)],
                                 states={0: [MessageHandler(Filters.text & ~Filters.command, add_product_by_url)]},
                                 fallbacks=[CommandHandler('cancel', cancel), CommandHandler('help', send_welcome),CommandHandler("delete_product", delete_product), CallbackQueryHandler(test_callback)],
                                 #per_message=True,
                                 allow_reentry= True,)

    dp.add_handler(adding)
    #dp.add_handler(CommandHandler("add_product", add_product))
    dp.add_handler(CommandHandler("delete_product", delete_product))
    dp.add_handler(CallbackQueryHandler(test_callback))
    updater.start_webhook(listen="0.0.0.0",
                          port=int(PORT),
                          url_path='1761092092:AAFrlihrNEI5akgickPXRvANRu7GUaWPVDE',
    webhook_url = 'https://monitoring-of-the-price.herokuapp.com/' + '1761092092:AAFrlihrNEI5akgickPXRvANRu7GUaWPVDE')
    # updater.bot.setWebhook('https://monitoring-of-the-price.herokuapp.com/' + '1761092092:AAFrlihrNEI5akgickPXRvANRu7GUaWPVDE')
    # updater.start_webhook(listen="0.0.0.0",
    #                       #port=int(PORT),
    #                       url_path='1793737971:AAGuNC3D_bK67060wJDhEoko2uSguMROMR0')
    #updater.start_polling()
    #updater.idle()

